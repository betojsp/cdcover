package main

import (
	"database/sql"
	"encoding/base64"
	"fmt"
	"html/template"
	"log"
	"net/http"

	_ "github.com/mattn/go-sqlite3"
)

type Scdaudio struct {
	SDTitle   string
	SDCdCover string
}

type Sartist struct {
	SDId     string
	SDArtist string
}

type Sgenre struct {
	SDId    string
	SDGenre string
}

func main() {
	fs := http.FileServer(http.Dir("./public"))

	http.Handle("/public/", http.StripPrefix("/public/", fs))
	http.HandleFunc("/genre/", GenreServer)
	http.HandleFunc("/artist/", ArtistServer)
	http.ListenAndServe(":8080", nil)
}

func GenreServer(w http.ResponseWriter, r *http.Request) {

	g, ok := r.URL.Query()["g"]

	if !ok || len(g[0]) < 1 {
		log.Println("Url Param 'gen' is missing")
		//return
	}

	gen := g[0]

	db, _ := sql.Open("sqlite3", "./cddb.db")
	defer db.Close()
	//
	// Load cdcovers filter to Genre
	//
	querySQL := "SELECT Title, CdCover FROM cdaudio WHERE Genre=" + string(gen)

	row, _ := db.Query(querySQL)

	var Title string
	var CdCover []byte

	var cdData []Scdaudio

	for row.Next() {
		row.Scan(&Title, &CdCover)
		cdData = append(cdData, Scdaudio{SDTitle: Title, SDCdCover: base64.StdEncoding.EncodeToString(CdCover)})
	}
	row.Close()

	//
	// Load Artists
	//
	querySQL = "SELECT id, Artist FROM artist ORDER BY Artist"

	row, _ = db.Query(querySQL)

	var Id string
	var Artist string

	var artData []Sartist

	for row.Next() {
		row.Scan(&Id, &Artist)
		artData = append(artData, Sartist{SDId: Id, SDArtist: Artist})
	}
	row.Close()

	//
	// Load Genre
	//
	querySQL = "SELECT id, Genre FROM genre ORDER BY Genre"

	row, _ = db.Query(querySQL)

	//var Id string
	var Genre string

	var genData []Sgenre

	for row.Next() {
		row.Scan(&Id, &Genre)
		genData = append(genData, Sgenre{SDId: Id, SDGenre: Genre})
	}
	row.Close()

	// Parse HTML
	parsedTemplate, _ := template.ParseFiles("./public/index.html")
	err := parsedTemplate.Execute(w, map[string]interface{}{"Art": artData, "Gen": genData, "CD": cdData})

	if err != nil {
		log.Println("Error executing template :", err)
		return
	}

	log.Println("Url Param 'gen' is: " + string(gen))
	fmt.Fprintf(w, "Url Param 'gen' is: %s", gen)
}

func ArtistServer(w http.ResponseWriter, r *http.Request) {

	a, ok := r.URL.Query()["a"]

	if !ok || len(a[0]) < 1 {
		log.Println("Url Param 'art' is missing")
		//return
	}

	art := a[0]

	db, _ := sql.Open("sqlite3", "./cddb.db")
	defer db.Close()
	//
	// Load cdcovers filter to Artists
	//
	querySQL := "SELECT Title, CdCover FROM cdaudio WHERE Artists=" + string(art)

	row, _ := db.Query(querySQL)

	var Title string
	var CdCover []byte

	var cdData []Scdaudio

	for row.Next() {
		row.Scan(&Title, &CdCover)
		cdData = append(cdData, Scdaudio{SDTitle: Title, SDCdCover: base64.StdEncoding.EncodeToString(CdCover)})
	}
	row.Close()

	//
	// Load Artists
	//
	querySQL = "SELECT id, Artist FROM artist ORDER BY Artist"

	row, _ = db.Query(querySQL)

	var Id string
	var Artist string

	var artData []Sartist

	for row.Next() {
		row.Scan(&Id, &Artist)
		artData = append(artData, Sartist{SDId: Id, SDArtist: Artist})
	}
	row.Close()

	//
	// Load Genre
	//
	querySQL = "SELECT id, Genre FROM genre ORDER BY Genre"

	row, _ = db.Query(querySQL)

	//var Id string
	var Genre string

	var genData []Sgenre

	for row.Next() {
		row.Scan(&Id, &Genre)
		genData = append(genData, Sgenre{SDId: Id, SDGenre: Genre})
	}
	row.Close()

	// Parse HTML
	parsedTemplate, _ := template.ParseFiles("./public/index.html")
	err := parsedTemplate.Execute(w, map[string]interface{}{"Art": artData, "Gen": genData, "CD": cdData})

	if err != nil {
		log.Println("Error executing template :", err)
		return
	}

	log.Println("Url Param 'art' is: " + string(art))
	fmt.Fprintf(w, "Url Param 'art' is: %s", art)
}
